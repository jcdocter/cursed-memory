<?php
    include "data/database.php";

    $sql = "SELECT user_id, username, SUM(score) AS amount_of_score, SUM(win) AS amount_of_win, DATE_FORMAT(date, '%d %M %Y') AS month FROM score INNER JOIN user ON score.user_id = user.id_user GROUP BY user_id ORDER BY amount_of_win desc LIMIT 5";

    if(!($result = $conn->query($sql))){
        showerror($conn->errno,$conn->error);
    }

    $result = $conn->query($sql);

    $row = $result->fetch_assoc();
    
    echo "<table>";
    do{        
        $lastMonth = Date("F", strtotime($row["month"]));

        echo "<tr>";
                echo "<td>";
                    echo "name: ". $row["username"];
                echo "</td>";

                echo "<td>";
                    echo "Score: ". $row["amount_of_score"];
                echo "</td>";

                echo "<td>";
                    echo "Wins: ". $row["amount_of_win"];
                echo "</td>";

                echo "<td>";
                    echo "Date: ". $row["month"];
                echo "</td>";

                echo "<td>";
                    echo "last month is: " . $lastMonth;;
                echo "</td>";

            echo "</tr>";

            echo json_encode($row);
    }
    while($row = $result->fetch_assoc());
    echo "</table>";
