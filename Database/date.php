<?php
    include "data/database.php";

    $sql = "SELECT * FROM score WHERE date >= DATE_SUB(NOW(), INTERVAL 1 MONTH)";

    if(!($result = $conn->query($sql))){
        showerror($conn->errno,$conn->error);
    }

    $result = $conn->query($sql);

    $row = $result->fetch_assoc();

    do{        
        echo json_encode($row);
    }
    while($row = $result->fetch_assoc());