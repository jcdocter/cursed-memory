<?php
    include "data/database.php";

    $sql = "SELECT name, score FROM score INNER JOIN users ON score.user_id = users.id ORDER BY score desc LIMIT 5";

    $result = $conn->query($sql);

    $row = $result->fetch_assoc();
    
    echo "<table>";
    do{        
        echo "<tr>";
                echo "<td>";
                    echo $row["name"];
                echo "</td>";

                echo "<td>";
                    echo $row["score"];
                echo "</td>";
            echo "</tr>";

            echo json_encode($row);
    }
    while($row = $result->fetch_assoc());
    echo "</table>";