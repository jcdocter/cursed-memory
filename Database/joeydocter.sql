-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Gegenereerd op: 17 jun 2022 om 08:46
-- Serverversie: 10.5.16-MariaDB
-- PHP-versie: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `joeydocter`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `score`
--

CREATE TABLE `score` (
  `id_score` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `win` tinyint(1) NOT NULL,
  `lose` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `server_id` int(11) NOT NULL,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `score`
--

INSERT INTO `score` (`id_score`, `score`, `win`, `lose`, `user_id`, `server_id`, `date`) VALUES
(1, 8, 1, 0, 1, 1, '2022-05-03'),
(2, 3, 1, 0, 1, 1, '2022-05-15'),
(3, 2, 0, 1, 2, 1, '2022-06-13'),
(4, 5, 0, 1, 3, 1, '2022-06-05'),
(5, 7, 1, 0, 4, 1, '2022-05-22');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `server`
--

CREATE TABLE `server` (
  `id_server` int(11) NOT NULL,
  `server_key` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `server`
--

INSERT INTO `server` (`id_server`, `server_key`) VALUES
(1, '4nw4R2RwkE');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `user`
--

INSERT INTO `user` (`id_user`, `username`, `email`, `password`) VALUES
(1, 'Jojo', 'joeydocter@gmail.com', 'NotASafePassword'),
(2, 'Haha', 'ohoh.Stinky@poopy.com', 'kjsdfklbdjhk'),
(3, 'odsnfj', 'dfgh.djfjgk@djkfnbhj.com', 'fvnkvfdknvfnvd'),
(4, 'Jan', 'jan.Jannsen@hotmail.com', '1qwsdr56yhjki90opl'),
(5, 'Vincent', 'vincent.booman@hku.nl', '1qsdr56yhjko0'),
(6, 'Ton', 'ton.markus@hku.nl', 'mhgtre32198uytfdx');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `score`
--
ALTER TABLE `score`
  ADD PRIMARY KEY (`id_score`);

--
-- Indexen voor tabel `server`
--
ALTER TABLE `server`
  ADD PRIMARY KEY (`id_server`);

--
-- Indexen voor tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `score`
--
ALTER TABLE `score`
  MODIFY `id_score` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT voor een tabel `server`
--
ALTER TABLE `server`
  MODIFY `id_server` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT voor een tabel `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
