using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using Newtonsoft.Json.Linq;
using UnityEngine.UI;
using TMPro;

public class Login : MonoBehaviour
{
    public TMP_InputField usernameText;
    public TMP_InputField passwordText;

    public TextMeshProUGUI serverKeyText;
    public TextMeshProUGUI warningText;


    private void Start()
    {
        StartCoroutine(StartServer());
        warningText.enabled = false;
    }

    private IEnumerator StartServer()
    {
        int id = 1;
        string password = "4nw4R2RwkE";

        using (UnityWebRequest www = UnityWebRequest.Get($"https://studenthome.hku.nl/~joey.docter/Netwerk/Database/server_login.php?id={id}&password={password}"))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);

                serverKeyText.text = $"Session: {www.downloadHandler.text}";
            }
        }
        yield return null;
    }

    public void LoginButton()
    {
        StartCoroutine(PlayerLogin());
    }

    private IEnumerator PlayerLogin()
    {
        string username = usernameText.text;
        string password = passwordText.text;
        using (UnityWebRequest www = UnityWebRequest.Get($"https://studenthome.hku.nl/~joey.docter/Netwerk/Database/user_login.php?username={username}&password={password}"))
        {
            yield return www.SendWebRequest();
            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);

                if (www.downloadHandler.text != "null")
                {
                    JObject json = JObject.Parse(www.downloadHandler.text);

                    if ((string)json["username"] == usernameText.text && (string)json["password"] == passwordText.text)
                    {
                        Debug.Log("Login Complete!");
                        PlayerPrefs.SetString("localUsername", (string)json["username"]);
                        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                    }
                    else
                    {
                        Debug.Log("username and/or password is incorrect. please try again.");
                        warningText.enabled = true;
                    }
                }
                else
                {
                    Debug.Log("username and/or password is empty.");
                    warningText.enabled = true;
                }
            }
        }
        yield return null;
    }
}
