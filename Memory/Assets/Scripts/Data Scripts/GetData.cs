using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GetData : MonoBehaviour
{
    public TextMeshProUGUI usernameText;

    void Start()
    {
        usernameText.text = PlayerPrefs.GetString("localUsername");
    }
}
