using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Networking.Transport;
using Unity.Collections;
using UnityEngine.UI;
using Unity.Networking.Transport.Utilities;
using UnityEngine.SceneManagement;
using System;

public class Client : MonoBehaviour
{
    private static Dictionary<NetworkMessageType, ClientMessageHandler> networkMessageHandlers = new Dictionary<NetworkMessageType, ClientMessageHandler> 
    {
        { NetworkMessageType.HANDSHAKE_RESPONSE, HandleServerHandshakeResponse },
        { NetworkMessageType.PLACE_CARDS, HandlePlaceCard },
        { NetworkMessageType.START_ROUND, HandleStartRound},
        { NetworkMessageType.UPDATE_CARD, HandleUpdateCard},
        { NetworkMessageType.RESULT,      HandleResult },
        { NetworkMessageType.PING, HandlePing }
    };

    public static string serverIP;
    public static string clientName = "";

    public GameManager gameManager;
    public NetworkDriver driver;
    public NetworkPipeline pipeline;
    public NetworkConnection connection;
    public bool done;

    private bool connected = false;
    private float startTime = 0;

    [System.Obsolete]
    private void Start()
    {
        if (Connection.isServer)
        {
            this.enabled = false;
        }

        startTime = Time.time;
        // Create connection to server IP
        driver = NetworkDriver.Create(new ReliableUtility.Parameters { WindowSize = 32 });
        pipeline = driver.CreatePipeline(typeof(ReliableSequencedPipelineStage));

        connection = default(NetworkConnection);

        var endpoint = NetworkEndPoint.Parse(serverIP, 9000, NetworkFamily.Ipv4);
        endpoint.Port = 1511;
        connection = driver.Connect(endpoint);
    }

    private void OnDestroy()
    {
        driver.Dispose();
    }

    private void Update()
    {
        driver.ScheduleUpdate().Complete();

       /* if (!connected && Time.time - startTime > 5f)
        {
            SceneManager.LoadScene(0);
        }*/

        if (!connection.IsCreated)
        {
            if (!done)
                Debug.Log("Something went wrong during connect");
            return;
        }

        DataStreamReader stream;
        NetworkEvent.Type cmd;
        while ((cmd = connection.PopEvent(driver, out stream)) != NetworkEvent.Type.Empty)
        {
            if (cmd == NetworkEvent.Type.Connect)
            {
                connected = true;
                Debug.Log("We are now connected to the server");

                // TODO: Create handshake message
                var header = new HandshakeMessage
                {
                    name = clientName
                };

                SendPackedMessage(header);
            }
            else if (cmd == NetworkEvent.Type.Data)
            {
                done = true;

                // First UInt is always message type (this is our own first design choice)
                NetworkMessageType msgType = (NetworkMessageType)stream.ReadUShort();

                // TODO: Create message instance, and parse data...
                MessageHeader header = (MessageHeader)System.Activator.CreateInstance(NetworkMessageInfo.TypeMap[msgType]);
                header.DeserializeObject(ref stream);

                if (networkMessageHandlers.ContainsKey(msgType))
                {
                    networkMessageHandlers[msgType].Invoke(this, header);
                }
                else
                {
                    Debug.LogWarning($"Unsupported message type received: {msgType}", this);
                }
            }
            else if (cmd == NetworkEvent.Type.Disconnect)
            {
                Debug.Log("Client got disconnected from server");
                connection = default(NetworkConnection);
            }
        }
    }

    public void SendPackedMessage(MessageHeader _header)
    {
        DataStreamWriter writer;
        int result = driver.BeginSend(pipeline, connection, out writer);

        // non-0 is an error code
        if (result == 0)
        {
            _header.SerializeObject(ref writer);
            driver.EndSend(writer);
        }
        else
        {
            Debug.LogError($"Could not wrote message to driver: {result}", this);
        }
    }

    static void HandleServerHandshakeResponse(Client _client, MessageHeader _header)
    {
        HandshakeResponseMessage response = _header as HandshakeResponseMessage;

        _client.gameManager.isLocal = true;
        _client.gameManager.isServer = false;
        _client.gameManager.AwakeObject();
    }

    static void HandlePlaceCard(Client _client, MessageHeader _header)
    {
        PlaceCardMessage placeCardMessage = _header as PlaceCardMessage;

        _client.gameManager.StartObjectInClient(placeCardMessage);

    }

    private static void HandleStartRound(Client _client, MessageHeader _header)
    {
        StartRoundMessage startRoundMessage = _header as StartRoundMessage;

        _client.gameManager.playersTurn = startRoundMessage.playerTurn;
    }

    private static void HandleUpdateCard(Client _client, MessageHeader _header)
    {
        UpdateCardMessage updateCardMessage = _header as UpdateCardMessage;
        _client.gameManager.deckList[(int)updateCardMessage.cardId].GetComponent<Card>().IsFlipped(updateCardMessage);
    }

    private static void HandleResult(Client _client, MessageHeader _header)
    {
        ResultMessage resultMessage = _header as ResultMessage;

        _client.gameManager.Result(resultMessage);
    }

    static void HandlePing(Client client, MessageHeader header)
    {
        Debug.Log("PING");

        PongMessage pongMsg = new PongMessage();
        client.SendPackedMessage(pongMsg);
    }
}
