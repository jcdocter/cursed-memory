using System.Collections;
using System.Collections.Generic;
using Unity.Networking.Transport;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class Connection : MonoBehaviour
{
    public static bool isServer = false;
    public TMP_InputField serverIPInput;

    public void ConnectedToServer()
    {
        isServer = true;

        GoToNextScene();
    }

    public void ConnectedAsClient()
    {
        isServer = false;

        NetworkEndPoint endPoint;
        if (NetworkEndPoint.TryParse(serverIPInput.text, 9000, out endPoint, NetworkFamily.Ipv4))
        {
            Client.serverIP = serverIPInput.text;
        }
        else
        {
            Client.serverIP = "127.0.0.1";
        }

        GoToNextScene();
    }

    private void GoToNextScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
