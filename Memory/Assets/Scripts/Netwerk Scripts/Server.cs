using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Networking.Transport;
using Unity.Collections;
using Unity.Networking.Transport.Utilities;
using System;

public static class NetworkMessageInfo
{
    public static Dictionary<NetworkMessageType, System.Type> TypeMap = new Dictionary<NetworkMessageType, System.Type> {
            { NetworkMessageType.HANDSHAKE,                 typeof(HandshakeMessage) },
            { NetworkMessageType.HANDSHAKE_RESPONSE,        typeof(HandshakeResponseMessage) },
            { NetworkMessageType.PLACE_CARDS,               typeof(PlaceCardMessage) },
            { NetworkMessageType.START_ROUND,               typeof(StartRoundMessage) },
            { NetworkMessageType.CHOOSE_CARD,               typeof(ChooseCardMessage) },
            { NetworkMessageType.UPDATE_CARD,               typeof(UpdateCardMessage) },
            { NetworkMessageType.RESULT,                    typeof(ResultMessage) },
        //    {NetworkMessageType.END_GAME,                   typeof(EndGameMessage) }
            { NetworkMessageType.PING,                      typeof(PingMessage) },
            { NetworkMessageType.PONG,                      typeof(PongMessage) }
        };
}

public delegate void ServerMessageHandler(Server server, NetworkConnection con, MessageHeader header);
public delegate void ClientMessageHandler(Client client, MessageHeader header);

public enum NetworkMessageType
{
    HANDSHAKE,
    HANDSHAKE_RESPONSE,
    SETUP,
    PLACE_CARDS,
    START_ROUND,
    CHOOSE_CARD,
    UPDATE_CARD,
    RESULT,
    END_GAME,
    PING,
    PONG
}

public class PingPong
{
    public float lastSendTime = 0;
    public int status = -1;
    public string name = ""; // because of weird issues...
}

public class Server : MonoBehaviour
{
    private static Dictionary<NetworkMessageType, ServerMessageHandler> networkMessageHandlers = new Dictionary<NetworkMessageType, ServerMessageHandler> 
    {
        { NetworkMessageType.HANDSHAKE,     HandleClientHandshake },
        { NetworkMessageType.CHOOSE_CARD,   HandleClientChooseCard },
        { NetworkMessageType.PONG,          HandleClientPong }
    };

    public GameManager gameManager;
    public MatchMaking match;

    public NetworkDriver m_Driver;
    public NetworkPipeline m_Pipeline;
    private NativeList<NetworkConnection> m_Connections;

    private Dictionary<NetworkConnection, string> nameList = new Dictionary<NetworkConnection, string>();
    private Dictionary<NetworkConnection, GameManager> playerList = new Dictionary<NetworkConnection, GameManager>();
    private Dictionary<NetworkConnection, PingPong> pongDict = new Dictionary<NetworkConnection, PingPong>();

    public static int amountOfPlayers;

    [System.Obsolete]
    void Start()
    {
        if (!Connection.isServer)
        {
            this.enabled = false;
        }

        // Create Driver
        m_Driver = NetworkDriver.Create(new ReliableUtility.Parameters { WindowSize = 32 });
        m_Pipeline = m_Driver.CreatePipeline(typeof(ReliableSequencedPipelineStage));

        // Open listener on server port
        NetworkEndPoint endpoint = NetworkEndPoint.AnyIpv4;
        endpoint.Port = 1511;
        if (m_Driver.Bind(endpoint) != 0)
            Debug.Log("Failed to bind to port 1511");
        else
            m_Driver.Listen();

        m_Connections = new NativeList<NetworkConnection>(64, Allocator.Persistent);
    }

    // Write this immediately after creating the above Start calls, so you don't forget
    //  Or else you well get lingering thread sockets, and will have trouble starting new ones!
    void OnDestroy()
    {
        m_Driver.Dispose();
        m_Connections.Dispose();
    }

    void Update()
    {
        // This is a jobified system, so we need to tell it to handle all its outstanding tasks first
        m_Driver.ScheduleUpdate().Complete();

        // Clean up connections, remove stale ones
        for (int i = 0; i < m_Connections.Length; i++)
        {
            if (!m_Connections[i].IsCreated)
            {
                m_Connections.RemoveAtSwapBack(i);
                // This little trick means we can alter the contents of the list without breaking/skipping instances
                --i;
            }
        }

        // Accept new connections
        NetworkConnection c;
        while ((c = m_Driver.Accept()) != default(NetworkConnection))
        {
            m_Connections.Add(c);
            Debug.Log("Accepted a connection");
        }

        DataStreamReader stream;
        for (int i = 0; i < m_Connections.Length; i++)
        {
            if (!m_Connections[i].IsCreated)
                continue;

            // Loop through available events
            NetworkEvent.Type cmd;
            while ((cmd = m_Driver.PopEventForConnection(m_Connections[i], out stream)) != NetworkEvent.Type.Empty)
            {
                if (cmd == NetworkEvent.Type.Data)
                {
                    // First UInt is always message type (this is our own first design choice)
                    NetworkMessageType msgType = (NetworkMessageType)stream.ReadUShort();

                    Debug.Log(msgType);

                    // Create instance and deserialize
                    MessageHeader header = (MessageHeader)System.Activator.CreateInstance(NetworkMessageInfo.TypeMap[msgType]);
                    header.DeserializeObject(ref stream);

                    if (networkMessageHandlers.ContainsKey(msgType))
                    {
                        try
                        {
                            networkMessageHandlers[msgType].Invoke(this, m_Connections[i], header);
                        }
                        catch
                        {
                            Debug.LogError($"Badly formatted message received: {msgType}");
                        }
                    }
                    else
                    {
                        Debug.LogWarning($"Unsupported message type received: {msgType}", this);
                    }
                }
            }
        }

        // Ping Pong stuff for timeout disconnects
        for (int i = 0; i < m_Connections.Length; i++)
        {
            if (!m_Connections[i].IsCreated)
                continue;

            if (pongDict.ContainsKey(m_Connections[i]))
            {
                if (Time.time - pongDict[m_Connections[i]].lastSendTime > 5f)
                {
                    pongDict[m_Connections[i]].lastSendTime = Time.time;
                    if (pongDict[m_Connections[i]].status == 0)
                    {
                        // Remove from all the dicts, save name / id for msg

                        // FIXME: for some reason, sometimes this isn't in the list?
                        if (nameList.ContainsKey(m_Connections[i]))
                        {
                            nameList.Remove(m_Connections[i]);
                        }

                        string name = pongDict[m_Connections[i]].name;
                        pongDict.Remove(m_Connections[i]);

                        // Disconnect this player
                        m_Connections[i].Disconnect(m_Driver);

                        // Clean up
                        m_Connections[i] = default;
                    }
                    else
                    {
                        pongDict[m_Connections[i]].status -= 1;
                        PingMessage pingMsg = new PingMessage();
                        SendUnicast(m_Connections[i], pingMsg);
                    }
                }
            }
            else if (nameList.ContainsKey(m_Connections[i]))
            { //means they've succesfully handshaked
                PingPong ping = new PingPong();
                ping.lastSendTime = Time.time;
                ping.status = 3;    // 3 retries
                ping.name = nameList[m_Connections[i]];
                pongDict.Add(m_Connections[i], ping);

                PingMessage pingMsg = new PingMessage();
                SendUnicast(m_Connections[i], pingMsg);
            }
        }
    }

    public void SendUnicast(NetworkConnection connection, MessageHeader header, bool realiable = true)
    {
        DataStreamWriter writer;
        int result = m_Driver.BeginSend(realiable ? m_Pipeline : NetworkPipeline.Null, connection, out writer);
        if (result == 0)
        {
            header.SerializeObject(ref writer);
            m_Driver.EndSend(writer);
        }
    }

    public void SendBroadcast(MessageHeader _header, NetworkConnection _exclude = default, bool _realiable = true)
    {
        for (int i = 0; i < m_Connections.Length; i++)
        {
            if (!m_Connections[i].IsCreated || m_Connections[i] == _exclude)
                continue;

            DataStreamWriter writer;
            int result = m_Driver.BeginSend(_realiable ? m_Pipeline : NetworkPipeline.Null, m_Connections[i], out writer);
            if (result == 0)
            {
                _header.SerializeObject(ref writer);
                m_Driver.EndSend(writer);
            }
        }
    }


    // Static handler functions
    //  - Client handshake                  (DONE)
    //  - Client chat message               (DONE)
    //  - Client chat exit                  (DONE)
    static void HandleClientHandshake(Server serv, NetworkConnection connection, MessageHeader header)
    {
        HandshakeMessage message = header as HandshakeMessage;

        HandshakeResponseMessage responseMsg = new HandshakeResponseMessage
        {
            message = "Match found"
        };

        serv.SendUnicast(connection, responseMsg);

        serv.nameList.Add(connection, message.name);
        serv.match.CreateMatch();

        amountOfPlayers += serv.match.CreateMatch();
        GameManager player = FindObjectOfType<GameManager>();
        player.isServer = true;
        player.isLocal = false;

        if(amountOfPlayers == 2)
        {
            StartRoundMessage startRoundMessage = new StartRoundMessage
            {
                playerTurn = false
            };

            serv.SendUnicast(connection, startRoundMessage);

            player.AwakeObject();
            player.StartObjectInServer();
            serv.playerList.Add(connection, player);

            Debug.Log("Game start");
            amountOfPlayers = 0;
        }

        if(amountOfPlayers == 1)
        {
            StartRoundMessage startRoundMessage = new StartRoundMessage
            {
                playerTurn = true
            };

            serv.SendUnicast(connection, startRoundMessage);
        }
    }

    private static void HandleClientChooseCard(Server _server, NetworkConnection _con, MessageHeader _header)
    {
        ChooseCardMessage chooseCardMessage = _header as ChooseCardMessage;

        for (int i = 0; i < _server.gameManager.deckList.Count; i++)
        {
           if(_server.gameManager.deckList[i].GetComponent<Card>().uniqueId == chooseCardMessage.cardId)
            {
                _server.gameManager.deckList[i].GetComponent<Card>().SelectCard(chooseCardMessage.selectedCard);
            }
        }
    }

    static void HandleClientPong(Server serv, NetworkConnection connection, MessageHeader header)
    {
        serv.pongDict[connection].status = 3;   //reset retry count
    }
}