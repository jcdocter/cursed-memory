using System.Collections;
using System.Collections.Generic;
using Unity.Networking.Transport;
using UnityEngine;

public class PlaceCardMessage : MessageHeader
{
    public override NetworkMessageType Type
    {
        get
        {
            return NetworkMessageType.PLACE_CARDS;
        }
    }

    public int cardId;
    public uint uniqueCardId;
    public int position;

    public override void SerializeObject(ref DataStreamWriter _writer)
    {
        base.SerializeObject(ref _writer);

        _writer.WriteInt(cardId);
        _writer.WriteUInt(uniqueCardId);
        _writer.WriteInt(position);
    }

    public override void DeserializeObject(ref DataStreamReader _reader)
    {
        base.DeserializeObject(ref _reader);

        cardId = _reader.ReadInt();
        uniqueCardId = _reader.ReadUInt();
        position = _reader.ReadInt();
    }
}
