using System.Collections;
using System.Collections.Generic;
using Unity.Networking.Transport;
using UnityEngine;

public class HandshakeResponseMessage : MessageHeader
{
    public override NetworkMessageType Type
    {
        get
        {
            return NetworkMessageType.HANDSHAKE_RESPONSE;
        }
    }

    public string message;
   // public uint networkID;

    public override void SerializeObject(ref DataStreamWriter _writer)
    {
        base.SerializeObject(ref _writer);

        _writer.WriteFixedString128(message);
      //  _writer.WriteUInt(networkID);
    }

    public override void DeserializeObject(ref DataStreamReader _reader)
    {
        base.DeserializeObject(ref _reader);

        message = _reader.ReadFixedString128().ToString();
      //  networkID = _reader.ReadUInt();
    }

}
