using System.Collections;
using System.Collections.Generic;
using Unity.Networking.Transport;
using UnityEngine;

public class CardPositionMessage : MessageHeader
{
    public override NetworkMessageType Type
    {
        get
        {
            return NetworkMessageType.SETUP;
        }
    }

    public int cardId;

    public override void SerializeObject(ref DataStreamWriter _writer)
    {
        base.SerializeObject(ref _writer);

        _writer.WriteInt(cardId);

    }

    public override void DeserializeObject(ref DataStreamReader _reader)
    {
        base.DeserializeObject(ref _reader);

        cardId = _reader.ReadInt();
    }
}
