using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Networking.Transport;

public class StartRoundMessage : MessageHeader
{
    public override NetworkMessageType Type
    {
        get
        {
            return NetworkMessageType.START_ROUND;
        }
    }

  //  public uint PlayerId;
    public bool playerTurn;

    public override void SerializeObject(ref DataStreamWriter _writer)
    {
        base.SerializeObject(ref _writer);

     //   _writer.WriteUInt(PlayerId);

        byte pt = Convert.ToByte(playerTurn);
        _writer.WriteByte(pt);
    }

    public override void DeserializeObject(ref DataStreamReader _reader)
    {
        base.DeserializeObject(ref _reader);

     //   PlayerId = _reader.ReadUInt();

        bool pt = Convert.ToBoolean(_reader.ReadByte());
        playerTurn = pt;
    }
}
