using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Networking.Transport;

public class ChooseCardMessage : MessageHeader
{
    public override NetworkMessageType Type
    {
        get
        {
            return NetworkMessageType.CHOOSE_CARD;
        }
    }

    public uint cardId;
    public SelectUpdate selectedCard;

    public override void SerializeObject(ref DataStreamWriter _writer)
    {
        base.SerializeObject(ref _writer);

        _writer.WriteUInt(cardId);
        _writer.WriteUShort((ushort)(selectedCard.turnCard ? 1 : 0));
    }

    public override void DeserializeObject(ref DataStreamReader _reader)
    {
        base.DeserializeObject(ref _reader);

        cardId = _reader.ReadUInt();
        selectedCard.turnCard = _reader.ReadUShort() != 0;

    }
}
