using System.Collections;
using System.Collections.Generic;
using Unity.Networking.Transport;
using UnityEngine;

public class HandshakeMessage : MessageHeader
{
    public override NetworkMessageType Type
    {
        get
        {
            return NetworkMessageType.HANDSHAKE;
        }
    }

    public string name = "";
    public uint networkId = 0;

    public override void SerializeObject(ref DataStreamWriter _writer)
    {
        base.SerializeObject(ref _writer);

        _writer.WriteFixedString128(name);
    }

    public override void DeserializeObject(ref DataStreamReader _reader)
    {
        base.DeserializeObject(ref _reader);

        name = _reader.ReadFixedString128().ToString();
    }
}
