using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Networking.Transport;
using UnityEngine;

public class ResultMessage : MessageHeader
{
    public override NetworkMessageType Type
    {
        get
        {
            return NetworkMessageType.RESULT;
        }
    }

    public bool hasWon;
    public int point;

    public override void SerializeObject(ref DataStreamWriter _writer)
    {
        base.SerializeObject(ref _writer);

        byte hw = Convert.ToByte(hasWon);
        _writer.WriteByte(hw);
        _writer.WriteInt(point);
    }

    public override void DeserializeObject(ref DataStreamReader _reader)
    {
        base.DeserializeObject(ref _reader);

        hasWon = Convert.ToBoolean(_reader.ReadByte());
        point = _reader.ReadInt();
    }
}
