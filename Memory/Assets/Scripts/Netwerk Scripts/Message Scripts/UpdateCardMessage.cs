using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Networking.Transport;


public class UpdateCardMessage : MessageHeader
{
    public override NetworkMessageType Type
    {
        get
        {
            return NetworkMessageType.UPDATE_CARD;
        }
    }

    public uint cardId;
    public bool isFlipped;

    public override void SerializeObject(ref DataStreamWriter _writer)
    {
        base.SerializeObject(ref _writer);

        _writer.WriteUInt(cardId);

        byte flipped = Convert.ToByte(isFlipped);
        _writer.WriteByte(flipped);
    }

    public override void DeserializeObject(ref DataStreamReader _reader)
    {
        base.DeserializeObject(ref _reader);

        cardId = _reader.ReadUInt();

        isFlipped = Convert.ToBoolean(_reader.ReadByte());
    }
}
