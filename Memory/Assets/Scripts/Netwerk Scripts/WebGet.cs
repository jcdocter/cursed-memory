using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json.Linq;

public class WebGet : MonoBehaviour
{
    private void Get()
    {
        StartCoroutine(GetCoroutine());
    }

    public IEnumerator GetCoroutine()
    {
        string url = "https://api.github.com/repos/hku-ect/gazebosc/pulls?state=all";

        using(UnityWebRequest www = UnityWebRequest.Get(url))
        {
            yield return www.SendWebRequest();

            if(www.error == null)
            {
                Debug.Log(www.downloadHandler.text);

                JArray arr = JArray.Parse(www.downloadHandler.text);

                Debug.Log(arr.Count);

                for (int i = 0; i < arr.Count; i++)
                {
                    JObject json = JObject.Parse(arr[i].ToString());

                    if((string)json["state"] == "open")
                    {
                        Debug.Log($"Found open pull: {json["title"]}, at: {json["html_url"]}");
                    }
                }
            }
        }

        yield return null;
    }

    private void Post()
    {
        StartCoroutine(PostCoroutine());
    }

    private IEnumerator PostCoroutine()
    {
        WWWForm form = new WWWForm();


        string username = "";
        string password = "";

        using(UnityWebRequest www = UnityWebRequest.Get($"http://localhost/database/user_login.php?name={username}&password={password}"))
        {
            yield return www.SendWebRequest();

            if(www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
        }

        yield return null;
    }
}
