using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json.Linq;
using TMPro;


public class Register : MonoBehaviour
{
    public TMP_InputField usernameText;
    public TMP_InputField emailText;
    public TMP_InputField passwordText;

    public TextMeshProUGUI warningText;

    public void RegisterButton()
    {
        StartCoroutine(RegisterPlayer());
    }

    private IEnumerator RegisterPlayer()
    {
        string username = usernameText.text;
        string email = emailText.text;
        string password = passwordText.text;
        using (UnityWebRequest www = UnityWebRequest.Get($"https://studenthome.hku.nl/~joey.docter/Netwerk/Database/insert_user.php?username={username}&email={email}&password={password}"))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
        }
        yield return null;
    }
}

