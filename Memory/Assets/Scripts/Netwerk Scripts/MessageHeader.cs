using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Networking.Transport;

public abstract class MessageHeader
{
    public static uint NextId = ++nextId;
    public abstract NetworkMessageType Type { get; }
    public uint ID { get; private set; } = NextId;

    private static uint nextId = 0;

    public virtual void SerializeObject(ref DataStreamWriter _writer)
    {
        _writer.WriteUShort((ushort)Type);
        _writer.WriteUInt(ID);
    }

    public virtual void DeserializeObject(ref DataStreamReader _reader)
    {
        ID = _reader.ReadUInt();
    }
}
