using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : NetworkBehaviour
{
    public bool playersTurn;
    public bool isServer;
    public bool isLocal;
    public bool hasWon;

    public GameObject[] cards;
    public List<Card> idList = new List<Card>();
    public List<GameObject> deckList = new List<GameObject>();

    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI opponentScoreText;
    public TextMeshProUGUI waitingText;
    public GridSpawner gridSpawner;

    private CardSpawner cardSpawner;

    private Client client;
    private Server server;

    private int score = 0;
    private int opponentScore = 0;
    private int point;

    public void AwakeObject()
    {
        waitingText.enabled = false;

        if (isLocal)
        {
            client = FindObjectOfType<Client>();
            cardSpawner = new CardSpawner(cards, gridSpawner, client);
        }

        if (isServer)
        {
            server = FindObjectOfType<Server>();
            cardSpawner = new CardSpawner(cards, gridSpawner, server);
        }
    }

    private void Update()
    {
        if (isLocal)
        {
            if (playersTurn)
            {
                StartPlaying();
            }
            else
            {
                Waiting();
            }
        }

        if (isServer)
        {
            Result();
        }
    }

    public void StartObjectInServer()
    {
        cardSpawner.InitializeCards();

        deckList = cardSpawner.cardList;
    }

    public void StartObjectInClient(PlaceCardMessage _pcm)
    {
        cardSpawner.InitializeCardsClient(_pcm);

        deckList.Add(cardSpawner.ReturnCards());
    }

    public void GetScore(int _point)
    {
        if (_point != 1)
        {
            return;
        }

        if (playersTurn)
        {
            score += _point;
            scoreText.text = score.ToString();
        }
        else
        {
            opponentScore += _point;
            opponentScoreText.text = opponentScore.ToString();
        }
        idList[1].gameObject.SetActive(false);
        idList[0].gameObject.SetActive(false);
    }

    public void Waiting()
    {
        Cursor.lockState = CursorLockMode.Locked;
        waitingText.enabled = true;
    }

    public void StartPlaying()
    {
        Cursor.lockState = CursorLockMode.None;
        waitingText.enabled = false;
    }

    public void Result()
    {
        if (server) 
        {
            if (idList.Count >= 2)
            {
                if (idList[0].id == idList[1].id)
                {
                    hasWon = true;
                    point = 1;

                    idList[1].gameObject.SetActive(false);
                    idList[0].gameObject.SetActive(false);
                    idList.Clear();
                }
                else
                {
                    hasWon = false;
                    point = 0;

                    idList[1].FlipToBack();
                    idList[0].FlipToBack();
                }

                ResultMessage resultMessage = new ResultMessage
                {
                    hasWon = this.hasWon,
                    point = this.point
                };

                server.SendBroadcast(resultMessage);
            }
        }      
    }

    public void Result(ResultMessage _rm)
    {
        if (_rm.hasWon)
        {
            GetScore(_rm.point);
        }
        else
        {
            if (playersTurn)
            {
                playersTurn = false;
            }
            else
            {
                playersTurn = true;
            }
        }

        idList.Clear();
    }
}
