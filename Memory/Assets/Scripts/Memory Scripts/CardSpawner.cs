using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardSpawner
{
    public List<GameObject> cardList = new List<GameObject>();
    private GameObject[] cards;
    private GameObject card;
    private Dictionary<int, int> cardDictionary = new Dictionary<int, int>();

    private GridSpawner gridSpawner;
    private Server server;

    private int gridNumber;
    private int amount;
    private uint id;

    public CardSpawner(GameObject[] _cards, GridSpawner _gridSpawner, Server _server)
    {
        this.cards = _cards;
        this.gridSpawner = _gridSpawner;
        this.server = _server;

        for (int i = 0; i < cards.Length; i++)
        {
            cardDictionary.Add(cards[i].GetComponent<Card>().id, cards[i].GetComponent<Card>().amount);
        }
    }

    public CardSpawner(GameObject[] _cards, GridSpawner _gridSpawner, Client _client)
    {
        cards = _cards;
        gridSpawner = _gridSpawner;
    }

    public void InitializeCards()
    {
        foreach (KeyValuePair<int, int> key in cardDictionary)
        {
            amount = 0;
            GameObject card = cards[key.Key];

            while (amount < key.Value)
            {
                gridNumber = Random.Range(0, 15);

                while (!gridSpawner.canPlaceList[gridNumber])
                {
                    gridNumber = Random.Range(0, 16);
                }

                PlaceCardMessage pcm = new PlaceCardMessage
                {
                    cardId = key.Key,
                    uniqueCardId = id,
                    position = gridNumber
                };

                server.SendBroadcast(pcm);

                GameObject c = GameObject.Instantiate(card, gridSpawner.gridList[gridNumber].transform.position, Quaternion.identity);
                c.GetComponent<Card>().uniqueId = id;
                cardList.Add(c);
                gridSpawner.canPlaceList[gridNumber] = false;
                amount++;
                id++;
            }
        }
    }

    public void InitializeCardsClient(PlaceCardMessage _pcm)
    {
        GameObject c = GameObject.Instantiate(cards[_pcm.cardId], gridSpawner.gridList[_pcm.position].transform.position, Quaternion.identity);
        c.GetComponent<Card>().uniqueId = _pcm.uniqueCardId;

        card = c;
    }

    public GameObject ReturnCards()
    {
        return card;
    }
}
