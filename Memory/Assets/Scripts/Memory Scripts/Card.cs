using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct SelectUpdate
{
    public bool turnCard;

    public SelectUpdate(bool _turnCard)
    {
        turnCard = _turnCard;
    }
}

public class Card : NetworkBehaviour
{ 
    public bool isServer;
    public bool isLocal;
    public bool isFlipped;
    public uint uniqueId;

    public int id;
    public int amount;

    public GameObject back;
    private GameManager gameManager;

    private Server server;
    private Client client;
    private SelectUpdate input;

    private bool startTimer;
    private bool canTurn;
    private float timer;
    private int timesFlip;

    private void Awake()
    {
        back.SetActive(true);
        gameManager = FindObjectOfType<GameManager>();
    }

    private void Start()
    {
        isLocal = gameManager.isLocal;
        isServer = gameManager.isServer;

        if (isLocal)
        {
            client = FindObjectOfType<Client>();
        }

        if (isServer)
        {
            server = FindObjectOfType<Server>();
        }
    }

    private void Update()
    {
        if (isServer)
        {
            if (input.turnCard && !isFlipped)
            {
                input.turnCard = false;
                isFlipped = true;
                FlipCard();

                UpdateCardMessage updateCardMessage = new UpdateCardMessage
                {
                    cardId = this.uniqueId,
                    isFlipped = isFlipped
                };

                server.SendBroadcast(updateCardMessage);
            }
        }

        if (isLocal)
        {
            SelectUpdate selectUpdate = new SelectUpdate(canTurn);

            if (gameManager.playersTurn && canTurn)
            {
                ChooseCardMessage chooseCardMessage = new ChooseCardMessage
                {
                    cardId = uniqueId,
                    selectedCard = selectUpdate
                };

                client.SendPackedMessage(chooseCardMessage);

                canTurn = false;
            }

            if (isFlipped && timesFlip <= 0)
            {
                FlipCard();
                timesFlip++;
            }

            if (!isFlipped)
            {
                this.back.SetActive(true);
                timesFlip = 0;
            }
        }
    }

    private void OnMouseDown()
    {
        if (isLocal)
        {
            if (Input.GetMouseButtonDown(0))
            {
                canTurn = Input.GetMouseButtonDown(0);
            }
        }
    }

    public void FlipCard()
    {
        gameManager.idList.Add(this);
        back.SetActive(false);
    }

    public void FlipToBack()
    {
        this.back.SetActive(true);
        isFlipped = false;

        UpdateCardMessage updateCardMessage = new UpdateCardMessage
        {
            cardId = this.uniqueId,
            isFlipped = isFlipped
        };

        server.SendBroadcast(updateCardMessage);

        gameManager.idList.Remove(this);
    }

    public void SelectCard(SelectUpdate _received)
    {
        input.turnCard = _received.turnCard;
    }

    public void IsFlipped(UpdateCardMessage _ucm)
    {
        isFlipped = _ucm.isFlipped;
    }
}
