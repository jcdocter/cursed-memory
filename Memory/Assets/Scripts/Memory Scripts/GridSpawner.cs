using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridSpawner : MonoBehaviour
{
    public int columnLenght;
    public int rowLength;

    public float xSpace;
    public float ySpace;

    public float startX;
    public float startY;

    public GameObject ground;

    public List<GameObject> gridList = new List<GameObject>();
    public List<bool> canPlaceList = new List<bool>();

    private void Awake()
    {
        for(int i = 0; i < columnLenght * rowLength; i++)
        {
          GameObject grid = GameObject.Instantiate(ground, new Vector3(startX + (xSpace * (i % columnLenght)), startY + (-ySpace * (i / columnLenght))), Quaternion.identity);

          gridList.Add(grid);
          canPlaceList.Add(true);
        }
    }
}
